Instructions to compile AppImage

1. Creare a directory
```	
	mkdir appimage
	cd appimage
```

2. git clone base in appimage
```	
	git clone https://gitlab.com/rebhu/rosf/thenexus-codes/scarlet/base
	cd base
	git checkout fix/upgrade
	cd ..
```

3. Compile and install scarlet using README

4. Download linuxdeploy and appimagetool appimages and make them executable

5. Run linuxdeploy to create AppDir
```	
	./linuxdeploy-x86_64.AppImage --appdir=AppDir --custom-apprun=base/appimage/AppRun --executable=base/bin/ga-client --library=/usr/lib/x86_64-linux-gnu/libpangoft2-1.0.so.0 --library=/usr/lib/x86_64-linux-gnu/libdatrie.so.1 --library=/usr/lib/x86_64-linux-gnu/libwayland-server.so.0 --desktop-file=base/appimage/scarlet.desktop --icon-file=base/appimage/scarlet.png --icon-filename=scarlet
```

6. Copy config files
```	
	cp -r base/bin/config AppDir/usr/share/
```

7. Create AppImage using appimagetool
```
	./appimagetool-x86_64.AppImage AppDir
```

8. Run AppImage
```	
	./scarlet-x86_64.AppImage
```


# copied
libpangocairo-1.0.so.0 libpango-1.0.so.0 libfribidi.so.0 libthai.so.0 libharfbuzz.so.0 libgbm.so.1

# in --library path
libpangoft2-1.0.so.0 libdatrie.so.1 libwayland-server.so.0

# ---------------------------------------------------------
## EXTRA

# apt install
apt install xorg

libgio-2.0.so.0
libEGL_mesa.so.0
libGLX_mesa.so.0
libdrm.so.2
libgbm.so.1
libpangocairo-1.0.so.0
libpango-1.0.so.0
libfontconfig.so.1
libpangoft2-1.0.so.0
libfribidi.so.0
libthai.so.0
libharfbuzz.so.0
libdatrie.so.1


./linuxdeploy-x86_64.AppImage --appdir=AppDir --custom-apprun=base/appimage/AppRun --executable=base/bin/ga-client --library=/usr/lib/x86_64-linux-gnu/libgio-2.0.so.0 --library=/usr/lib/x86_64-linux-gnu/libEGL_mesa.so.0 --library=/usr/lib/x86_64-linux-gnu/libGLX_mesa.so.0 --library=/usr/lib/x86_64-linux-gnu/libdrm.so.2 --library=/usr/lib/x86_64-linux-gnu/libgbm.so.1 --library=/usr/lib/x86_64-linux-gnu/libpangocairo-1.0.so.0 --library=/usr/lib/x86_64-linux-gnu/libpango-1.0.so.0 --library=/usr/lib/x86_64-linux-gnu/libdatrie.so.1 --library=/usr/lib/x86_64-linux-gnu/dri/i965_dri.so --desktop-file=base/appimage/scarlet.desktop --icon-file=base/appimage/scarlet.png --icon-filename=scarlet
