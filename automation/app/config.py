from pydantic import BaseSettings
from dotenv import load_dotenv

load_dotenv(verbose=True)

class Settings(BaseSettings):
    
    aws_region: str
    hosted_zone_id: str
    api_key: str

    class Config:
        env_file = ".env"

settings = Settings()